/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Foreground and Background Palette Picker Module v0.1.0
 *
 * Foreground and Background Palette Picker Module is a JavaScript module that can be used to create a color picker for
 * applying two-color schemes to given HTML targets.
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
class ForegroundAndBackgroundPalettePicker {
    constructor(window, colorPalette, defaultColorObject = {
        background: "#ffffff",
        foreground: "#000000"
    }) {
        if (!(window instanceof Window))
            throw "The window must be an instance of Window.";

        this._window = window;
        this.colorPalette = colorPalette;
        this.defaultColorObject = defaultColorObject;
    }

    get colorPalette() {
        return this._colorPalette;
    }

    set colorPalette(colorPalette) {
        ForegroundAndBackgroundPalettePicker.validateColorPalette(colorPalette);

        this._colorPalette = colorPalette;
    }

    get defaultColorObject() {
        return this._defaultColorObject;
    }

    set defaultColorObject(defaultColorObject) {
        ForegroundAndBackgroundPalettePicker.validateColorObject(defaultColorObject);

        this._defaultColorObject = defaultColorObject;
    }

    createPalettePicker(tableIdentifier, foregroundTargetSelector, backgroundTargetSelector,
                        fixedWrapperBackgroundColor = "transparent", captionForegroundColor = "#000000",
                        stackOrderIndex = 1, prependTargetSelector = "body",
                        targetForegroundHorizontalLineRules = true) {
        if (typeof tableIdentifier !== "string")
            throw "The value for the table identifier must be a string.";

        if (typeof foregroundTargetSelector !== "string")
            throw "The value for the foreground target must be a valid string selector.";

        if (typeof backgroundTargetSelector !== "string")
            throw "The value for the background target must be a valid string selector.";

        if (typeof fixedWrapperBackgroundColor !== "string")
            throw "The value for the background color of the fixed wrapper must be a string.";

        if (typeof captionForegroundColor !== "string")
            throw "The value for the foreground color of the caption must be a string.";

        if (!Number.isInteger(stackOrderIndex) || stackOrderIndex < 0 || stackOrderIndex > 2147483647)
            throw "The value of the index that specifies the stack level of the fixed wrapper must be an integer that" +
            " is greater than or equal to 0 and less than or equal to 2147483647";

        if (typeof prependTargetSelector !== "string")
            throw "The value for the prepend target must be a valid string selector that represents a DOM element, to" +
            " which the palette picker will be prepended.";

        if (typeof targetForegroundHorizontalLineRules !== "boolean")
            throw "The value of the parameter for determining whether or not the border colors of horizontal rules" +
            " contained in the foreground should be targeted must be either true or false.";

        const windowDocument = this._window.document;
        const fixedWrapper = windowDocument.createElement("div");
        const table = windowDocument.createElement("table");
        const tableCaption = windowDocument.createElement("caption");
        const tableHeader = windowDocument.createElement("thead");
        const tableBody = windowDocument.createElement("tbody");
        const tableFooter = windowDocument.createElement("tfoot");
        const tableHeaderRow = windowDocument.createElement("tr");
        const tableBackgroundRow = windowDocument.createElement("tr");
        const tableForegroundRow = windowDocument.createElement("tr");
        const tableFooterRow = windowDocument.createElement("tr");
        const tableFootCell = windowDocument.createElement("td");
        const tableBackgroundRowHeader = windowDocument.createElement("th");
        const tableForegroundRowHeader = windowDocument.createElement("th");
        const foregroundTarget = windowDocument.querySelector(foregroundTargetSelector);
        const backgroundTarget = windowDocument.querySelector(backgroundTargetSelector);
        const keys = Object.keys(this._colorPalette);
        const colorCount = keys.length;
        const fixedWrapperStyle = fixedWrapper.style;
        const tableStyle = table.style;
        const tableCaptionStyle = tableCaption.style;
        const tableHeaderStyle = tableHeader.style;
        const tableBodyStyle = tableBody.style;
        const tableFooterStyle = tableFooter.style;
        const tableBackgroundRowHeaderStyle = tableBackgroundRowHeader.style;
        const tableForegroundRowHeaderStyle = tableForegroundRowHeader.style;
        const tableHeaderRowStyle = tableHeaderRow.style;
        const tableBackgroundRowStyle = tableBackgroundRow.style;
        const tableForegroundRowStyle = tableForegroundRow.style;
        const tableFooterRowStyle = tableFooterRow.style;
        const tableFootCellStyle = tableFootCell.style;

        fixedWrapperStyle.position = "fixed";
        fixedWrapperStyle.top = "1em";
        fixedWrapperStyle.width = "100%";
        fixedWrapperStyle.opacity = "0";
        fixedWrapperStyle.zIndex = stackOrderIndex.toString();
        fixedWrapperStyle.minHeight = "200px";
        fixedWrapperStyle.backgroundColor = fixedWrapperBackgroundColor;

        table.setAttribute("id", tableIdentifier);

        tableStyle.display = "none";
        tableStyle.margin = "0 auto";
        tableStyle.padding = "4px";
        tableStyle.backgroundColor = "#fff";
        tableStyle.border = "1px solid #000";
        tableStyle.borderRadius = "3px";
        tableStyle.borderCollapse = "separate";
        tableStyle.borderSpacing = "2px";

        tableCaptionStyle.display = "table-caption";
        tableCaptionStyle.padding = "0.25em";
        tableCaptionStyle.textAlign = "center";
        tableCaptionStyle.color = captionForegroundColor;

        tableCaption.appendChild(windowDocument.createTextNode("Palette Picker"));

        tableHeaderStyle.display = "table-header-group";
        tableHeaderStyle.verticalAlign = "middle";

        tableBodyStyle.display = "table-row-group";
        tableBodyStyle.verticalAlign = "middle";

        tableFooterStyle.display = "table-footer-group";
        tableFooterStyle.verticalAlign = "middle";

        tableBackgroundRowHeaderStyle.display = "table-cell";
        tableBackgroundRowHeaderStyle.verticalAlign = "inherit";
        tableBackgroundRowHeaderStyle.fontWeight = "bold";
        tableBackgroundRowHeaderStyle.textAlign = "center";

        tableBackgroundRowHeader.appendChild(windowDocument.createTextNode("b"));

        tableForegroundRowHeaderStyle.display = "table-cell";
        tableForegroundRowHeaderStyle.verticalAlign = "inherit";
        tableForegroundRowHeaderStyle.fontWeight = "bold";
        tableForegroundRowHeaderStyle.textAlign = "center";

        tableForegroundRowHeader.appendChild(windowDocument.createTextNode("f"));

        tableHeaderRowStyle.display = "table-row";
        tableHeaderRowStyle.verticalAlign = "inherit";

        tableBackgroundRowStyle.display = "table-row";
        tableBackgroundRowStyle.verticalAlign = "inherit";

        tableBackgroundRow.appendChild(tableBackgroundRowHeader);

        tableForegroundRowStyle.display = "table-row";
        tableForegroundRowStyle.verticalAlign = "inherit";

        tableForegroundRow.appendChild(tableForegroundRowHeader);

        tableFooterRowStyle.display = "table-row";
        tableFooterRowStyle.verticalAlign = "inherit";

        for (let i = 0; i <= colorCount; i++) {
            const cell = windowDocument.createElement("th");
            const cellStyle = cell.style;

            cellStyle.display = "table-cell";
            cellStyle.verticalAlign = "inherit";
            cellStyle.fontWeight = "bold";
            cellStyle.textAlign = "center";

            if (i === 0)
                cell.appendChild(windowDocument.createTextNode("\u00A0"));
            else
                cell.appendChild(windowDocument.createTextNode(`${i}`));

            tableHeaderRow.appendChild(cell);
        }

        tableHeader.appendChild(tableHeaderRow);

        const createColorCells = (tableColorRow) => {
            for (let i = 0; i < colorCount; i++) {
                const cell = windowDocument.createElement("td");
                const cellStyle = cell.style;

                cellStyle.width = "35px";
                cellStyle.height = "35px";
                cellStyle.display = "table-cell";
                cellStyle.verticalAlign = "inherit";
                cellStyle.cursor = "pointer";

                cell.appendChild(windowDocument.createTextNode("\u00A0"));

                tableColorRow.appendChild(cell);
            }
        };

        createColorCells(tableBackgroundRow);
        createColorCells(tableForegroundRow);

        tableBody.appendChild(tableBackgroundRow);
        tableBody.appendChild(tableForegroundRow);

        tableFootCellStyle.display = "table-cell";
        tableFootCellStyle.textAlign = "center";
        tableFootCellStyle.verticalAlign = "inherit";

        tableFootCell.setAttribute("colspan", `${colorCount + 1}`);
        tableFootCell.appendChild(windowDocument.createTextNode("\u00A0"));

        tableFooterRow.appendChild(tableFootCell);

        tableFooter.appendChild(tableFooterRow);

        table.appendChild(tableCaption);
        table.appendChild(tableHeader);
        table.appendChild(tableBody);
        table.appendChild(tableFooter);

        const tableRows = tableBody.querySelectorAll("tr");
        const backgroundRow = [...tableRows][0];
        const foregroundRow = [...tableRows][1];
        const backgroundRowData = backgroundRow.querySelectorAll("td");
        const foregroundRowData = foregroundRow.querySelectorAll("td");
        const tableFooterCellOutputTarget = tableFooter.querySelector("tr > td");
        const addStyleToHorizontalLineRules = (targetForegroundHorizontalLineRules, colorObject) => {
            if (targetForegroundHorizontalLineRules) {
                const horizontalLineRules = foregroundTarget.querySelectorAll("hr");

                [...Array.from(horizontalLineRules)].forEach(horizontalLineRule => horizontalLineRule.style.borderColor
                    = colorObject.foreground);
            }
        };
        const handlePaletteMouseLeaveEvent = () => {
            backgroundTarget.style.backgroundColor = this._defaultColorObject.background;

            foregroundTarget.style.color = this._defaultColorObject.foreground;

            addStyleToHorizontalLineRules(targetForegroundHorizontalLineRules, this._defaultColorObject);
        };

        for (let i = 0; i < colorCount; i++) {
            const key = keys[i];
            const currentBackgroundRowData = backgroundRowData[i];
            const currentForegroundRowData = foregroundRowData[i];

            currentBackgroundRowData.style.backgroundColor = this._colorPalette[key].background;

            currentForegroundRowData.style.backgroundColor = this._colorPalette[key].foreground;

            const handlePaletteMouseEnterEvent = () => {
                backgroundTarget.style.backgroundColor = this._colorPalette[key].background;

                foregroundTarget.style.color = this._colorPalette[key].foreground;

                tableFooterCellOutputTarget.innerText = "";
                tableFooterCellOutputTarget.appendChild(windowDocument.createTextNode(`b/f: \
${this._colorPalette[key].background}/${this._colorPalette[key].foreground}`));

                addStyleToHorizontalLineRules(targetForegroundHorizontalLineRules, this._colorPalette[key]);
            };

            currentBackgroundRowData.addEventListener("mouseleave", handlePaletteMouseLeaveEvent);
            currentBackgroundRowData.addEventListener("mouseenter", handlePaletteMouseEnterEvent);

            currentForegroundRowData.addEventListener("mouseleave", handlePaletteMouseLeaveEvent);
            currentForegroundRowData.addEventListener("mouseenter", handlePaletteMouseEnterEvent);
        }

        fixedWrapper.appendChild(table);

        windowDocument.querySelector(prependTargetSelector).prepend(fixedWrapper);

        const handleMouseLeaveEvent = (event) => {
            const eventTarget = event.target;
            const eventTargetStyle = eventTarget.style;

            table.style.display = "none";

            eventTargetStyle.opacity = "0";
            eventTargetStyle.transition = null;

            handlePaletteMouseLeaveEvent();
        };
        const handleTouchEndEvent = (event) => {
            const eventTarget = event.target;

            let eventToDispatch;

            if (eventTarget.style.transition === "opacity 1s ease 0s")
                eventToDispatch = "mouseleave";
            else
                eventToDispatch = "mouseenter";

            eventTarget.dispatchEvent(new Event(eventToDispatch));
        };
        const handleMouseEnterEvent = (event) => {
            const eventTarget = event.target;
            const eventTargetStyle = eventTarget.style;

            table.style.display = "table";

            eventTargetStyle.opacity = null;
            eventTargetStyle.transition = "opacity 1s ease 0s";
        };

        fixedWrapper.addEventListener("mouseleave", handleMouseLeaveEvent);
        fixedWrapper.addEventListener("touchend", handleTouchEndEvent);
        fixedWrapper.addEventListener("mouseenter", handleMouseEnterEvent);
    }

    static validateColorPalette(colorPalette) {
        if (typeof colorPalette !== "object" || colorPalette === null)
            throw "The color palette must be an object.";

        const keys = Object.keys(colorPalette);

        if (keys.length === 0)
            throw "The color palette must have at least one key that maps to a valid color object.";

        keys.forEach(key => ForegroundAndBackgroundPalettePicker.validateColorObject(colorPalette[key]));
    }

    static validateColorObject(colorObject) {
        if (typeof colorObject !== "object" || colorObject === null)
            throw "An invalid color object was encountered.";

        if (!Object.prototype.hasOwnProperty.call(colorObject, "background"))
            throw "Each color object must contain a background property.";

        if (typeof colorObject["background"] !== "string")
            throw "Each background property must be a string.";

        if (!Object.prototype.hasOwnProperty.call(colorObject, "foreground"))
            throw "Each color object must contain a foreground property.";

        if (typeof colorObject["foreground"] !== "string")
            throw "Each foreground property must be a string.";
    }
}

export default ForegroundAndBackgroundPalettePicker;