# Foreground and Background Palette Picker Module

## About

### Author

Ryan E. Anderson

---

### Description

Foreground and Background Palette Picker Module is a JavaScript module that can be used to create a color picker for
evaluating the presentation of a two-color scheme after it has been applied to the foreground and background of a
given HTML target.

- Foreground and Background Palette Picker Module exports a class that has behavior for adding a color picker to a web 
page.
- Foreground and Background Palette Picker Module can be used in a mobile context.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## State

### window

This state variable stores the global window object of a certain browser context.

### colorPalette

This object defines the color palette that will be used by the picker. Each key of this object must be paired with 
another object that contains at least the following two properties: background and foreground. The keys or properties of 
the parent object may have any unique name.

### defaultColorObject

This is the default color scheme of the target. This scheme is used when the picker is in a
hidden or disabled state.

## Constructor

### constructor(window, colorPalette, defaultColorObject)

This constructor can be used to initialize each state variable. This constructor has three parameters, one of which has 
a default value.

#### Parameters

##### window

This parameter is used to assign a global window object from a certain browser context to a corresponding state 
variable.

##### colorPalette

This parameter is used to assign a color palette to a corresponding state variable.

##### defaultColorObject

This parameter is used to assign a default color palette to a corresponding state variable. This parameter has a default 
color map, which contains #FFFFFF for the background and #000000 for the foreground.

## Behavior

### createPalettePicker(tableIdentifier, foregroundTargetSelector, backgroundTargetSelector, fixedWrapperBackgroundColor, captionForegroundColor, stackOrderIndex, prependTargetSelector, targetForegroundHorizontalLineRules)

This method is used to add a palette picker to a web page.

#### Parameters

##### tableIdentifier

This value sets the identifier of the table that stores the color schemes.

##### foregroundTargetSelector

This value is used to set an HTML target, the foreground of which will be styled when a user activates a certain color 
scheme by entering a cell of the table that stores the palette.

##### backgroundTargetSelector

This value is used to set an HTML target, the background of which will be styled when a user activates a certain color 
scheme by entering a cell of the table that stores the palette.

##### fixedWrapperBackgroundColor

This value is used to set the background color of the fixed wrapper that contains the palette. The default value for 
this parameter is transparent.

##### captionForegroundColor

This value is used to set the foreground color of the caption of the table that contains the palette. The default value 
for this parameter is #000000.

##### stackOrderIndex

This value specifies the stack level of the fixed wrapper that contains the palette. This value must be a number that is 
greater than or equal to 0 and less than or equal to 2147483647. The default stack level is 1.

##### prependTargetSelector

This value determines where the color picker will be prepended. The default target is the body element of an HTML 
document.

##### targetForegroundHorizontalLineRules

This value determines whether or not the border colors of horizontal rules contained in the foreground should be 
targeted. The default value for this parameter is true.

## Using Foreground and Background Palette Picker Module in a Project

### User Interaction

Two-color schemes can be evaluated by simply moving a pointer into the region of a web page where this module has been 
installed. Pick different two-color schemes by continuing to move the pointer over the palette that appears after 
entering the region of a web page where this module has been installed. For mobile contexts, tapping the screen of a 
device near the region of a web page where this module has been installed will make the palette appear or disappear.

### Installation

Foreground and Background Palette Picker Module can be used in a project by importing it inside of a script element of 
type module.

Below is sample markup of a script element containing the use of the module (See demo.).

```html
<script type="module">
    import ForegroundAndBackgroundPalettePicker from "./js/foreground-and-background-palette-picker-module";

    const colorPalette = {
        firstColorSet: {background: "#ffffff", foreground: "#000000"},
        secondColorSet: {background: "#0095a3", foreground: "#a32c00"},
        thirdColorSet: {background: "#000000", foreground: "#000000"},
        fourthColorSet: {background: "#f5f5dc", foreground: "#195c86"},
        fifthColorSet: {background: "#778899", foreground: "#000000"},
        sixthColorSet: {background: "#a32c00", foreground: "#00626b"},
    };

    const foregroundAndBackgroundPalettePicker = new ForegroundAndBackgroundPalettePicker(window, colorPalette);

    foregroundAndBackgroundPalettePicker.createPalettePicker("#palette", "#wrapper", "body");
</script>
```

## Demo

A static HTML file is provided to evaluate the module. To use the demo, deploy all contents from the root of this 
project to a directory on a web server; and make any necessary mappings for virtual hosts.

### Support

The demo is not supported by legacy contexts because of the modern version of ECMAScript that is used in the 
implementations of a module and a script. Also, the markup and style sheet are not compatible with some older contexts.